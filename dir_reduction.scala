object DirReduction {
  
  def dirReduc(arr: Array[String]): Array[String] = {
    
    val cancels = Set(("NORTH", "SOUTH"), ("SOUTH", "NORTH"), 
                      ("EAST", "WEST"), ("WEST", "EAST"))
    
    if (arr.length < 2) arr
    else {
      for (i <- 0 until arr.length-1) {
        if (cancels(arr(i), arr(i+1))) return dirReduc(arr.slice(0, i) ++ arr.slice(i+2, arr.length))
      }
      arr
    }
  }
}